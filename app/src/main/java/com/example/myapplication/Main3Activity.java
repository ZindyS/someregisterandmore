package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity {

    LinearLayout lay;
    EditText edit;
    Button btn;

    SharedPreferences sh;
    SharedPreferences.Editor ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        sh = getSharedPreferences("Bruh", Context.MODE_PRIVATE);
        ed = sh.edit();
        lay = findViewById(R.id.lay1);
        lay.setBackgroundResource(sh.getInt("color", R.color.colorAccent));
        btn = findViewById(R.id.button8);
        edit = findViewById(R.id.editText);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(edit.getText())) {
                    ed.putInt("stat", 1);
                    ed.putString("text", String.valueOf(edit.getText()));
                    ed.commit();
                    Intent intent = new Intent(Main3Activity.this, Main4Activity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
}
