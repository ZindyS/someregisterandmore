package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.icu.text.CaseMap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.appbar.AppBarLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Main4Activity extends AppCompatActivity {

    EditText username, password;
    LinearLayout lay;

    SharedPreferences sh;

    TextView title;

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://intelligent-system.online")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        sh = getSharedPreferences("Bruh", Context.MODE_PRIVATE);
        username = findViewById(R.id.editText2);
        password = findViewById(R.id.editText3);
        lay = findViewById(R.id.lay2);
        lay.setBackgroundResource(sh.getInt("color", R.color.colorAccent));
        title = findViewById(R.id.title);
        title.setText(sh.getString("text", "Error404"));
    }

    public void onClicked(View v) {
        if (!TextUtils.isEmpty(username.getText()) && !TextUtils.isEmpty(password.getText())) {
            SomeAPI api = retrofit.create(SomeAPI.class);
            api.login(new User(String.valueOf(username.getText()), String.valueOf(password.getText()))).enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.isSuccessful()) {
                        Intent intent = new Intent(Main4Activity.this, Main6Activity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(Main4Activity.this, response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Toast.makeText(Main4Activity.this, "Error404", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(this, "Поле(я) пустое(ые)", Toast.LENGTH_SHORT).show();
        }
    }

    public void onTextClicked(View v) {
        Intent intent = new Intent(Main4Activity.this, Main5Activity.class);
        startActivity(intent);
    }
}
