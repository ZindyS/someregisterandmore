package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Main5Activity extends AppCompatActivity {

    EditText username, password, email, card;
    LinearLayout lay;

    //Стандартное создание ретрофита
    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://intelligent-system.online")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    SharedPreferences sh;

    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);
        sh = getSharedPreferences("Bruh", Context.MODE_PRIVATE);
        //Присваивание айдишек к едиттекстам
        username = findViewById(R.id.editText4);
        password = findViewById(R.id.editText5);
        email = findViewById(R.id.editText6);
        card = findViewById(R.id.editText7);
        lay = findViewById(R.id.lay123);
        lay.setBackgroundResource(sh.getInt("color", R.color.colorAccent));
        title = findViewById(R.id.title1);
        title.setText(sh.getString("text", "Error404"));
    }


    //Сама регистрация
    public void onRegClicked(View v) {
        //Проверка на пустоту
        if (!TextUtils.isEmpty(username.getText()) && !TextUtils.isEmpty(password.getText()) && !TextUtils.isEmpty(email.getText()) && !TextUtils.isEmpty(card.getText())) {
            //Проверка на наличие знака @ в поле почта
            char[] text = String.valueOf(email.getText()).toCharArray();
            Boolean bool = false;
            for(int i = 0; i < text.length; i++) {
                if (text[i] == '@') {
                    bool = true;
                    break;
                }
            }
            if (bool) {
                //Довольно схожая ситуация, что и с аторизацией, только используется конструктор не
                //с двумя тестами(username и password), а с четырьмя(username, password, email, card)
                //Ещё пару различий можно найти в SomeAPI(фактически, там меняется только ссылка) и
                //User(там как раз и находятся конструкторы)
                SomeAPI api = retrofit.create(SomeAPI.class);
                api.registr(new User(String.valueOf(username.getText()), String.valueOf(password.getText()), String.valueOf(email.getText()), String.valueOf(card.getText()))).enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(Main5Activity.this, "Успешная регистрация!", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Main5Activity.this, Main4Activity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(Main5Activity.this, response.message(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {

                    }
                });
            } else {
                Toast.makeText(this, "Введите правильный Email!", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Поле(я) пустое(ые)", Toast.LENGTH_SHORT).show();
        }
    }

    public void onTexttClicked(View v) {
        Intent intent = new Intent(Main5Activity.this, Main4Activity.class);
        startActivity(intent);
    }
}
