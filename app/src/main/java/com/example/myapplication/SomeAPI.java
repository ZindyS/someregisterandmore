package com.example.myapplication;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SomeAPI {
    @POST("/api/signin")
    Call<User> login(@Body User u);
    @POST("/api/signup")
    Call<User> registr(@Body User u);
}
